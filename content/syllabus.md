---
title: "Syllabus"
date: 2018-08-25T14:09:23+03:00
draft: false
---


# Course Syllabus

## Description
The objective of this course is to familiarize students with the skills and concepts of Human-Computer Interaction (HCI), including the understanding of user needs, interface design and prototyping, and interface evaluation. Topics include an introduction to HCI, HCI goals, cognitive and perceptual issues, HCI design, data gathering, data analysis, task description, task analysis, interaction styles, interaction frameworks, prototyping, and evaluation.

**Credits:** 2 credit hours
**Prerequisite:** CPCS-204

## Course Learning Outcomes (CLO):
					
By completion of the course the students should be able to:

- Identify good and bad elements in interfaces.
- List and explain numbers of usability goals.
- Analyse different type of usability principles.	
- Demonstrate an understanding of usability principles in illustrating good and bad interface.
- Apply the model of information processing.
- Identify the impact of human memory on interface design.
- Identify the activities in HCI design process.
- Differentiate between design rules, design standards, and design guidelines.
- Explain the Shneiderman’s eight golden rules.
- Differentiate between several data gathering methods.
- Identify the needs for a user interface.			
- Differentiate between qualitative and quantitative data.
- Use different data analysis tool to analyze gathered data.
- Design prototypes at varying levels of fidelity, from paper prototypes to functional, interactive prototypes.
- Employ selected evaluation methods at a basic level of competence to report errors.
- Use task analysis methods to analyze a user goal.


## Textbook and References
- Yvonne Rogers, Helen Sharp, Jenny Preece, , "Interaction Design", John Wiley & Sons; 3 edition (2011-06-07) ISBN-13 9780470665763 ISBN-10 0470665769
- Additional reading materials will be provided by the instructor.

## Grading
- Assignments: 10%
- Project: 35%
- Lab: 5%
- Midterm exam: 15%
- Final exam: 35%
