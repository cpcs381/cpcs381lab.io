---
title: "Assignment 3"
date: 2018-08-25T22:54:38+03:00
draft: false
---

## Assignment 3 <span class="tag is-info is-medium assignment-due-date">Due date: Thursday October 11th at 2:00 PM</span>

Q1. Read the usability.gov's article titled [Personas](https://www.usability.gov/how-to-and-tools/methods/personas.html) and answer the following two questions:

- a) What are user personas and why do we need them?
- b) Write a Persona for an app that you use.

Q2. Read the article titled [Creating Good Interview and Survey Questions](https://owl.purdue.edu/owl/research_and_citation/conducting_research/conducting_primary_research/interview_and_survey_questions.html)
and write three examples of bad questions and explain how they can be fixed and why.

Submit a PDF file of your answers on Slack in private as a direct message (DM) by the due date

### Grading
The assignment is graded on a scale of 100. The following grading criteria is used in grading your assignments.

|Score	   | Meaning  |
|---------|----------|
|100-90   |  Excellent.	The submission demonstrates high mastery of concepts and techniques. This implies that your answer is articulate, thorough, complete, and not brief.|
| 80-89  | Good. The submission meets most of the requirements but did not provide adequate answer or fails to elaborate more and explain the answer. This is typical for short answers that avoid going into necessary details that address the main point in the question.|
|  70-79 | Close to satisfactory.	The submission is close to satisfactory but misses important details and did not meet all the requirements.|
|  60-69 | Less than satisfactory	. The submission requires significant improvements. |
|  50-59 |Unsatisfactory. The submission has some incorrect answer or does not meet the requirements. Assignments graded within this range demonstrates low level of understanding to the requirements and concepts. |
| 0-49   | Unacceptable. The submission is mostly incorrect or did not meet the requirements and demonstrates the student’s inability to demonstrate a level of understanding to the concepts in the assignment.|


**CLO: 7 and 11; SO: b and f**