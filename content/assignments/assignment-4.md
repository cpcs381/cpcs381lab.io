---
title: "Assignment 4"
date: 2018-08-25T22:54:40+03:00
draft: false
---

## Assignment 4 <span class="tag is-info is-medium assignment-due-date">Due date: Sunday October 4th at 2:00 PM</span>
Watch the following [three video series of Google's design team](https://www.youtube.com/watch?v=JMjozqJS44M&list=PL9KVIdeJ2K8NDpsiyYpcbB_qifd3y5CYZ) and answer the following questions:

- What is rapid prototyping?
- When to use paper, digital, and native prototyping?
- Describe something that surprises you or you have found it to be interesting.

### Grading
The assignment is graded on a scale of 100. The following grading criteria is used in grading your assignments.

|Score	   | Meaning  |
|---------|----------|
|100-90   |  Excellent.	The submission demonstrates high mastery of concepts and techniques. This implies that your answer is articulate, thorough, complete, and not brief.|
| 80-89  | Good. The submission meets most of the requirements but did not provide adequate answer or fails to elaborate more and explain the answer. This is typical for short answers that avoid going into necessary details that address the main point in the question.|
|  70-79 | Close to satisfactory.	The submission is close to satisfactory but misses important details and did not meet all the requirements.|
|  60-69 | Less than satisfactory	. The submission requires significant improvements. |
|  50-59 |Unsatisfactory. The submission has some incorrect answer or does not meet the requirements. Assignments graded within this range demonstrates low level of understanding to the requirements and concepts. |
| 0-49   | Unacceptable. The submission is mostly incorrect or did not meet the requirements and demonstrates the student’s inability to demonstrate a level of understanding to the concepts in the assignment.|

**CLO: 7, 14, and 15;  SO: b and e**