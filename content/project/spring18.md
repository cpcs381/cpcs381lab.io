---
title: "Spring18"
date: 2018-08-25T14:42:47+03:00
draft: false
---
## CPCS 381 Spring 2018

![Spring 2018 class](/images/class-spring-18-demo-1.jpg)
![Spring 2018 class](/images/class-spring-18-demo-2.jpg)

### Best Projects
#### 1- Fahmni (tutor app)
<iframe src="https://player.vimeo.com/video/286669344" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<iframe src="https://player.vimeo.com/video/286669553" width="640" height="368" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


#### 2- Parky app (parking app)
![Spring 2018 Parky app](/images/spring_2018_parky_app.png)

<iframe src="https://player.vimeo.com/video/286672163" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>



