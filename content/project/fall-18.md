---
title: "Project - Fall 2018"
date: 2018-08-25T14:10:02+03:00
draft: false
---


# CPCS 381, Fall 2018 - Project Description
## Description
This project will help you learn and apply the fundamental concepts of user-centered design and usability. You’ll design an application (web or mobile) that solves a problem for a particular group of users. You will apply usability testing and conduct experiments, iterate through your design, and refine the design of your application. Successful completion of this project will provide you with an understanding of the iterative design process.

## Objectives
- Be able to conduct formative research using qualitative and quantitative methods to gain information about the target users and feasibility of the idea.
- Learn how to conduct usability testing, collect data, and interpret results.
- Learn how to build low-fidelity prototypes using papers.
- Be able to compare design alternatives using flexible, fast, and low-cost methods.
- Learn how to conduct usability testing with users to test low-fidelity prototypes.
- Learn how to build medium-fidelity prototypes using interactive user interface prototype tools.
- Be able to revise the initial low-fidelity prototype and incorporate the feedback received during testing with potential users.
- Compare and contrast medium-fidelity prototyping with low-fidelity prototyping.
- Learn how to build a high-fidelity prototype with an experience close to the native interface.
- Be able to evaluate and communicate your design ideas.

## Activities and Stages
This project is divided into four main stages:

- [Stage 0](#stage-0-introduction)
- [Stage 1](#stage-1-understanding-your-users)
- [Stage 2](#stage-2-low-fidelity-prototypes)
- [Stage 3](#stage-3-interactive-medium-fidelity-prototypes)
- [Stage 4](#stage-4-final-report-refined-prototype-video-and-presentation)

## Ideas
Here are some ideas from previous semesters

- [Spring 2018](/project/spring18)

### Stage 0: Introduction
**Due on** Wednesday 19/9/2018 at 11:59 PM [Week 3]
**Grade:** None (Accept/Reject)
#### Description
You will need to form a team of 3-4 students to design a mobile or web application that solves a problem for a particular group of users. You will write a description of the problem your team is trying to solve. You should include at least an example of a related project/app and why you think it did not solve the problem you’re trying to solve. You should also describe what your project will do differently to address these problems. Your project idea will be reviewed for approval by your instructor.
#### What to submit?
A report that includes:

- A list of the group members. Your team should also agree on a group leader who will be responsible for submitting the reports and will act as the primary contact for your group.
- A clear description of the problem, examples of related projects, and how your app is different.

### Stage 1: Understanding your users
**Due on**  Thursday 11/10/2018 at 11:59 PM [Week ~~5~~ 6]
**Grade:** 20% of the project’s grade (7 grades)
#### Description
You will conduct formative research to understand your users, their problems, and validate the assumption that your app is a reasonable solution. You will identify your target users using some inclusion and exclusion criteria. You will conduct formative research with your target users. Your group is expected to use two types of methods (e.g., interviews, ethnographic study, survey/questionnaire research, direct observation). If you have questions about the appropriate method for your app, you may talk to your instructor. You will then conduct your formative user research with at least four users (not people in this class).
#### What to do?
- Identify your users (write personas and scenarios).
- Pick two research methods to conduct your user research.
- Inform users about the study (obtain consent,  explain goals, the length of the study, and protocol you will follow).
- Conduct two studies using two methods.
- Collect the data (take notes, record audio, or use online questionnaire, etc.).
- Analyze the data to interpret your findings (visualize your findings (e.g., graphs), discuss themes and patterns, etc.).
- Write your user research report.
#### What to submit?
- A report (10-15 pages) that includes the following:
  - Introduction: briefly describe your project and goals.
  - Target users: Identify your target users. Describe at least two personas for your app and three main scenarios for each persona.
  - Two research methods: Describe the methods you used to understand your users? Why you chose each method?
  - The study you conducted (e.g. the protocol of the study, research questions)
  - The results from each participant. Please make sure they remain anonymous <sup id="s1a">[1](#f1a)</sup>.
  - Findings: What did you find in your research? How did you collect and analyze the results? What surprises you?
  - Conclusion: What did you learn from this activity?
- Participation summary: In one paragraph, each team member must describe how each member contributed to this stage and submit it to the instructor in private (on blackboard).

<b id="f1a">*1</b> You must include the demographics of your users for each study you conducted (e.g., age, education, technology background, etc).[↩](#s1a)

### Stage 2: Low-Fidelity Prototypes
**Due on** Thursday 1/11/2018 at 11:59PM [Week ~~7~~ 8]
**Grade:** 25% of the project’s grade (8.75 grades)

#### Description
You will produce three paper prototypes showing two alternative approaches for your design. These two alternative designs should differ in how the users interact with your interface. For example, comparing a horizontal navigation bar that appears at the top of an app screen versus a vertical navigation bar that appears at the left of an app screen is not considered a difference in user interaction. They simply do not affect the way the user interacts with your app. What you should compare instead are two distinct and more interesting design alternatives. For example, comparing a collapsible navigation panel (a.k.a navigation drawer or slide-out navigation) versus tabs with swipe elements is more interesting and considered a difference in user interaction. You will be testing these prototypes with your potential users. Your paper prototype should reflect the findings you’ve found in stage 1.
#### What to do?
- Create three complete paper prototypes that show two alternative approaches for the essential functionality of your app. That’s it, three prototypes with A and B prototypes (i.e., 6 paper prototypes in total). For example, for a paper prototype for navigation, you will create a prototype A and prototype B.
- Come up with a list of user tasks to use in your user evaluation study. For example, navigate from home to X and select Y, or add an item to your shopping cart, etc.
- Conduct a user evaluation with your A and B prototypes with at least three users.

#### What to submit?
- Digital copies of your A and B paper prototypes. This can be pictures or scanned images of your paper prototypes.
- A report that includes the following:
  - Write a paragraph describing the difference in your A and B prototypes.
  - Write a list of your user tasks for testing your prototype.
  - Describe your testing protocol, method, and report on subjective feedback from your users and usability problems that came up during your test.
  - Participation summary: In one paragraph, each team member must describe how each member contributed to this report and submit that to the instructor in private (on blackboard).

### Stage 3: Interactive Medium-Fidelity Prototypes
**Due on** Thursday 15/11/2018 at 11:59 PM [Week 11]
**Grade:** 25% of the project’s grade (8.75 grades)
#### Description
You will use interactive tools to build medium-fidelity prototypes. You will have to use the results of your low-fidelity prototype tests and instructor’s feedback to revise the design of your interfaces. You will need to focus on the essential tasks of your application and ignore implementation details of your application. Your prototype must show at least three important tasks of your application and the changes you made on your paper prototype. The prototype should demonstrate improvement over the paper prototypes. You may not make A and B mid-fi prototypes, if you have already made your decision and consolidated your choices.
#### What prototyping tool to use?
You may use one of the following tools: marvelapp, proto.io, invisionapp, justinmind, or origami. If you prefer to use a different tool, please talk to your instructor first.
#### What to do?
- You will need to use the paper prototypes from the previous activities and incorporate the feedback you’ve received during your user testing session.
- You will design a revised version of the paper prototype using a mid-fidelity prototype tool.
- You will conduct usability test for your mid-fidelity prototype with at least four potential users (not people in this class).
- You will collect the results of your usability testing and report on that.
#### What to submit?
- A report that includes the following:
  - A link to your mid-fidelity prototype. The link should be public and accessible by anyone. If you use a tool that does not provide sharing on the cloud (e.g., origami), you may use services such as DropBox or Google Drive to host your design prototypes.
  - A paragraph explaining how your mid-fi prototypes have improved over the previous paper prototypes.
  - One page usability testing report listing any usability problems discovered while testing the prototype with potential users of your application.
- Participation summary: In one paragraph, each team member must describe how each member contributed to this report and submit that to the instructor in private (on blackboard).

### Stage 4: Final Report, Refined Prototype, Video, and Presentation
**Due on** Sunday 02/12/2018 at 02:00 PM [Week 14]
**Grade:** 30% of the project’s grade (10.5 grades)

#### Description
In this stage, you will need to address the issues discovered in stage 3. You’ll have to refine your prototype based on the feedback and findings of your mid-fi prototypes user testing. Your team will create the final refined mid-fidelity prototype. You will also need to create a video explaining the goal and importance of your app. You need to create a plan for a good video that communicates your ideas to new users. Thus, you need to create a Storyboard to show a roadmap of your video and help your team create a great promotional video for your app. Your team will share the video and present the final prototype to the class.

#### What to do?
- Refine your mid-fi design based on the feedback and results of your user testing in stage 3.
- Draw a storyboard for your video. It should demonstrate why your app is needed and how a user can perform some tasks with your app.
- Create a video for your app. The video may be a real life video of a target user of your app or a slideshow of your prototypes. Your video will be 5 minutes maximum.
- Prepare a five minutes presentation outlining the changes you made to the design of your app as well as the strengths and limitations of your app. You should conclude with future directions and work for your project.

#### What tool to use to make the video?
You may use [Powtoon](https://www.powtoon.com) or a screencast software like [Camtasia](https://www.techsmith.com/video-editor.html) or any other tool you prefer.

#### What to submit?
- Link to the final revised interactive prototype.
- Scanned images or pictures of your paper storyboard.
- Link to your video. The video must be publicly accessible. You may use services such as YouTube, or Google Drive.
- Your presentation slides or a link to them (e.g., Google Slides).

## Grading Criteria 
This project is worth 35% of your final grade. The following grading criteria is used in grading your deliverables for each stage of the project:


<div class="table-container">
        <table class="table table-container is-fullwidth is-bordered">
            <thead>
                <tr>
                    <th>Score</th>
                    <th>Meaning</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>5</td>
                    <td>Excellent</td>
                    <td>The submission demonstrates high mastery of concepts and techniques.</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Good</td>
                    <td>The submission meets most of the requirements but did not provide adequate coverage of the requirements.</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Close to satisfactory</td>
                    <td>The submission is close to satisfactory but misses important details and did not meet all the requirements.</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Less than satisfactory</td>
                    <td>The submission requires significant improvements.</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Unsatisfactory</td>
                    <td>The submission does not meet the requirements and demonstrates low level of understanding to the requirements and concepts.</td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>Unacceptable</td>
                    <td>The submission did not meet the requirements and demonstrates the student’s inability to demonstrate a level of understanding to all concepts.</td>
                </tr>
            </tbody>
        </table>
    </div>


<div class="table-container">
    <table class="table table-container is-fullwidth is-bordered">
        <thead>
            <tr>
                <th>Stage</th>
                <th>Evaluation Criteria</th>
                <th>0</th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td>1</td>
            <td>Demonstration of an adequate level of understanding of the target users and demonstration of skills in data collection, analysis, interpretation, and presenting findings.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td>2</td>
            <td>Making low-fidelity prototypes and conducting a usability testing that shows an adequate coverage of the main tasks of the interface.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td>3</td>
            <td>Making interactive mid-fidelity prototypes with appropriate interface revisions​ based on user testing feedback and testing them with users.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td>4</td>
            <td>Presentation and final report that demonstrates an understanding of the iterative design process.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
        </tbody>
    </table>
</div>


